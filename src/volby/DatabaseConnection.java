package volby;

import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DatabaseConnection {

    private static final String CONNECTION = "jdbc:postgresql://krizik.felk.cvut.cz:5434/student_db15_22";
    private static final String USERNAME = "student_db15_22";
    private static final String PASSWORD = "tomashonza";
    private Connection conn;

    public DatabaseConnection() throws ClassNotFoundException, SQLException {
        this.conn = DriverManager.getConnection(CONNECTION, USERNAME, PASSWORD);
    }

    public Connection getConnection() {
        return this.conn;
    }

    public static void showDBErrorDialog(JFrame parent) {
        JOptionPane.showMessageDialog(parent,
                "Selhání databáze.",
                "Error",
                JOptionPane.ERROR_MESSAGE);
    }
}
