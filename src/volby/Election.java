package volby;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import javax.persistence.*;

@Entity
@Table(name = "volby")
public class Election implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "volby_id")
    private Integer id;
    @Column(name = "zac", nullable = false)
    private Date start;
    @Column(name = "kon", nullable = false)
    private Date end;
    @Column(name = "typ_voleb", nullable = false)
    private String type;

    public static Election createElection(Date start, Date end, String type) {
        Election newElection = new Election();
        newElection.setStart(start);
        newElection.setEnd(end);
        newElection.setType(type);
        return newElection;
    }

    public static ArrayList<Election> getCurrElections(Connection conn) throws SQLException {
        ArrayList<Election> currElections = new ArrayList<>();
        PreparedStatement ps = conn.prepareStatement(
                "SELECT volby_id, zac, kon, typ_voleb "
                + "FROM volby "
                + "WHERE zac <= current_date "
                + "AND kon >= current_date");
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            currElections.add(loadElection(rs));
        }
        return currElections;
    }

    public static ArrayList<Election> getFinishedElections(Connection conn) throws SQLException {
        ArrayList<Election> currElections = new ArrayList<>();
        PreparedStatement ps = conn.prepareStatement(
                "SELECT volby_id, zac, kon, typ_voleb "
                + "FROM volby "
                + "WHERE kon < current_date");
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            currElections.add(loadElection(rs));
        }
        return currElections;
    }

    private static Election loadElection(ResultSet rs) throws SQLException {
        Election election = new Election();
        election.setId(rs.getInt("volby_id"));
        election.setType(rs.getString("typ_voleb"));
        election.setStart(rs.getDate("zac"));
        election.setEnd(rs.getDate("kon"));
        return election;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(start);
        int elecYear = cal.get(Calendar.YEAR);
        return type.trim() + " volby " + elecYear;
    }
}
