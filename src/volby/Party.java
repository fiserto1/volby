package volby;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.persistence.*;

@Entity
@Table(name = "strana")
public class Party implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "strana_id")
    private Integer id;
    @Column(name = "nazev", nullable = false)
    private String name;

    public static Party createParty(String name) {
        Party newParty = new Party();
        newParty.setName(name);
        return newParty;
    }

    public static ArrayList<Party> getParties(Connection conn, Election elections, Area area) throws SQLException {
        ArrayList<Party> parties = new ArrayList<>();
        PreparedStatement ps = conn.prepareStatement("SELECT strana_id, nazev "
                + "FROM vysl_strany JOIN strana ON strana_stranaid = strana_id "
                + "WHERE volby_volbyid = ? AND uzemi_uzemid = ?");
        ps.setInt(1, elections.getId());
        ps.setInt(2, area.getId());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            parties.add(loadParty(rs));
        }
        return parties;
    }

    private static Party loadParty(ResultSet rs) throws SQLException {
        Party party = new Party();
        party.setId(rs.getInt("strana_id"));
        party.setName(rs.getString("nazev"));
        return party;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name.trim();
    }
}
