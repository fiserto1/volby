package volby;

import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.*;

public class LoginDialog extends JDialog {

    private JTextField tfUsername = new JTextField(20);
    private JPasswordField pfPassword = new JPasswordField(20);
    private JLabel lUsername = new JLabel("ID uživatele: ");
    private JLabel lPassword = new JLabel("Heslo: ");
    private JButton butLogin = new JButton("Přihlásit se");
    private JButton butCancel = new JButton("Zrušit");
    private boolean succeeded;
    private Connection conn;
    private JFrame parent;

    public LoginDialog(JFrame parent, Connection conn) {

        super(parent, "Login", true);
        this.conn = conn;
        this.parent = parent;
        
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();

        cs.fill = GridBagConstraints.HORIZONTAL;

        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(lUsername, cs);

        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 2;
        panel.add(tfUsername, cs);

        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(lPassword, cs);

        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        panel.add(pfPassword, cs);
        panel.setBorder(new LineBorder(Color.GRAY));

        butLogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                loginActionPerformed(e);
            }
        });
        pfPassword.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                loginActionPerformed(e);
            }
        });
        butCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel bp = new JPanel();
        bp.add(butLogin);
        bp.add(butCancel);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }

    private void loginActionPerformed(ActionEvent evt) {

        int userID;
        try {
            userID = Integer.parseInt(getUsername());
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(LoginDialog.this,
                    "Špatné ID uživatele",
                    "Login",
                    JOptionPane.ERROR_MESSAGE);

            tfUsername.setText("");
            pfPassword.setText("");
            succeeded = false;
            return;
        }

        try {
            if (authenticate(userID, getPassword())) {
                succeeded = true;
                new InsertFrame(parent, conn, userID).setVisible(true);
                dispose();
            } else {
                JOptionPane.showMessageDialog(LoginDialog.this,
                        "Špatné ID uživatele nebo heslo.",
                        "Login",
                        JOptionPane.ERROR_MESSAGE);

                tfUsername.setText("");
                pfPassword.setText("");
                succeeded = false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(LoginDialog.this,
                    "Selhání databáze.",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(LoginDialog.class.getName()).log(Level.SEVERE, null, ex);
            tfUsername.setText("");
            pfPassword.setText("");
            succeeded = false;
        }
    }

    private boolean authenticate(int username, String password) throws SQLException {

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT verify_login(" + username
                + "," + (password.hashCode() + username) + ")");
        rs.next();
        return rs.getBoolean("verify_login");

    }

    public String getUsername() {
        return tfUsername.getText().trim();
    }

    public String getPassword() {
        return new String(pfPassword.getPassword());
    }

    public boolean isSucceeded() {
        return succeeded;
    }
}
