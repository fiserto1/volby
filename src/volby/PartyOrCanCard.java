package volby;

import java.awt.LayoutManager;
import javax.swing.JPanel;

/**
 *
 * @author Tomas
 */
public class PartyOrCanCard extends JPanel {

    private int cardID;
    private int partyOrCanID;
    private Area area;
    private int numOfVotes;
    private boolean clicked = false;

    public PartyOrCanCard(int cardID, int partyOrCanID, Area area, int numOfVotes, LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
        this.cardID = cardID;
        this.partyOrCanID = partyOrCanID;
        this.area = area;
        this.numOfVotes = numOfVotes;
    }

    public PartyOrCanCard(int cardID, int partyOrCanID, Area area, int numOfVotes, LayoutManager layout) {
        super(layout);
        this.cardID = cardID;
        this.partyOrCanID = partyOrCanID;
        this.area = area;
        this.numOfVotes = numOfVotes;
    }

    public PartyOrCanCard(int cardID, int partyOrCanID, Area area, int numOfVotes, boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        this.cardID = cardID;
        this.partyOrCanID = partyOrCanID;
        this.area = area;
        this.numOfVotes = numOfVotes;
    }

    public PartyOrCanCard(int cardID, int partyOrCanID, Area area, int numOfVotes) {
        this.cardID = cardID;
        this.partyOrCanID = partyOrCanID;
        this.area = area;
        this.numOfVotes = numOfVotes;
    }

    public int getCardID() {
        return cardID;
    }

    public void setCardID(int cardID) {
        this.cardID = cardID;
    }

    public int getPartyOrCanID() {
        return partyOrCanID;
    }

    public void setPartyOrCanID(int partyOrCanID) {
        this.partyOrCanID = partyOrCanID;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public int getNumOfVotes() {
        return numOfVotes;
    }

    public void setNumOfVotes(int numOfVotes) {
        this.numOfVotes = numOfVotes;
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }
}
