package volby;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tomas
 */
public class MainFrame extends javax.swing.JFrame {

    private Connection conn;

    /**
     * Creates new form NewJFrame
     *
     * @param conn
     */
    public MainFrame(Connection conn) {
        this.conn = conn;
        initComponents();
        setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panTitle = new javax.swing.JPanel();
        lTitle = new javax.swing.JLabel();
        panBody = new javax.swing.JPanel();
        panButtons = new javax.swing.JPanel();
        butResults = new javax.swing.JButton();
        butInsert = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lTitle.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lTitle.setText("Volby");
        panTitle.add(lTitle);

        getContentPane().add(panTitle, java.awt.BorderLayout.NORTH);

        panBody.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 20, 20));

        panButtons.setLayout(new java.awt.GridLayout(0, 1, 0, 10));

        butResults.setText("Výsledky voleb");
        butResults.setPreferredSize(new java.awt.Dimension(200, 25));
        butResults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butResultsActionPerformed(evt);
            }
        });
        panButtons.add(butResults);

        butInsert.setText("Zadávání výsledků");
        butInsert.setPreferredSize(new java.awt.Dimension(200, 25));
        butInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butInsertActionPerformed(evt);
            }
        });
        panButtons.add(butInsert);

        panBody.add(panButtons);

        getContentPane().add(panBody, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void butResultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butResultsActionPerformed
        try {
            ResultsFrame resFrame = new ResultsFrame(this, conn);
            resFrame.setVisible(true);
            setVisible(false);
        } catch (SQLException ex) {
            DatabaseConnection.showDBErrorDialog(this);
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_butResultsActionPerformed

    private void butInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butInsertActionPerformed
        LoginDialog loginDlg = new LoginDialog(this, conn);
        loginDlg.setVisible(true);
        if (loginDlg.isSucceeded()) {
            setVisible(false);
        }
    }//GEN-LAST:event_butInsertActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton butInsert;
    private javax.swing.JButton butResults;
    private javax.swing.JLabel lTitle;
    private javax.swing.JPanel panBody;
    private javax.swing.JPanel panButtons;
    private javax.swing.JPanel panTitle;
    // End of variables declaration//GEN-END:variables
}
