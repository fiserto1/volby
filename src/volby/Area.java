package volby;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.persistence.*;

@Entity
@Table(name = "uzemi")
public class Area implements Serializable {

    @Id //@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "uzemi_id")
    private Integer id;
    @Column(name = "nazev", nullable = false)
    private String name;
    @Column(name = "typ_uzemi_naz", nullable = false)
    private String type;

    public static Area createArea(String name, String type) {
        Area newArea = new Area();
        newArea.setName(name);
        newArea.setType(type);
        return newArea;
    }

    //pouze o jeden level nizsi
    //pro areaID = 1(CR) vraci kraje i senatni obvody (rozlisit podle typu uzemi)
    public static ArrayList<Area> getLowerAreas(Connection conn, Area area) throws SQLException {
        ArrayList<Area> lowerAreas = new ArrayList<>();
        PreparedStatement ps = conn.prepareStatement(
                "SELECT uzemi_id, nazev, typ_uzemi_naz "
                + "FROM level_lower(?)");
        ps.setInt(1, area.getId());
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            lowerAreas.add(loadArea(rs));
        }
        return lowerAreas;
    }

    public static ArrayList<Area> getAllLowerAreas(Connection conn, Area area) throws SQLException {
        ArrayList<Area> lowerAreas = new ArrayList<>();
        PreparedStatement ps = conn.prepareStatement(
                "SELECT uzemi_id, nazev, typ_uzemi_naz "
                + "FROM find_all_lowers(?)");
        ps.setInt(1, area.getId());
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            lowerAreas.add(loadArea(rs));
        }
        return lowerAreas;
    }

    private static Area loadArea(ResultSet rs) throws SQLException {
        Area area = new Area();
        area.setId(rs.getInt("uzemi_id"));
        area.setName(rs.getString("nazev"));
        area.setType(rs.getString("typ_uzemi_naz"));
        return area;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return name.trim();
    }
}
