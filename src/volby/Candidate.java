package volby;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "kandidat")
public class Candidate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "kandidat_id")
    private Integer id;

    @JoinColumn(name = "osoba_osobaid", nullable = false)
    @ManyToOne
    private Person person;

    @JoinColumn(name = "strana_stranaid", nullable = false)
    @ManyToOne
    private Party party;

    @JoinColumn(name = "volby_volbyid", nullable = false)
    @ManyToOne
    private Election election;

    @JoinColumn(name = "uzemi_uzemid", nullable = false)
    @ManyToOne
    private Area area;

    @Column(name = "popis", nullable = false)
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public Election getElection() {
        return election;
    }

    public void setElection(Election election) {
        this.election = election;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
