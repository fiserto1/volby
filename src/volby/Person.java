package volby;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "osoba")
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "osoba_id")
    private Integer id;

    @Column(name = "jmeno", nullable = false)
    private String firstname;

    @Column(name = "prijmeni", nullable = false)
    private String surname;

    @JoinColumn(name = "uzemi_uzemid", nullable = false)
    @ManyToOne
    private Area area;

    public static Person createPerson(String firstname, String surname, Area area) {
        Person newPerson = new Person();
        newPerson.setFirstname(firstname);
        newPerson.setSurname(surname);
        newPerson.setArea(area);
        return newPerson;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }
}
