package volby;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tomas
 */
public class SetUserPassword {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        DatabaseConnection db = new DatabaseConnection();
        final Connection conn = db.getConnection();

        int id = 6;
        String psw = "test";

        Statement st = conn.createStatement();
        try {
            st.execute("UPDATE osoba SET heslo = "
                    + (psw.hashCode() + id) + " WHERE osoba_id = " + id);
            System.out.println("Heslo ulozeno.");
        } catch (SQLException ex) {
            
            Logger.getLogger(SetUserPassword.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        conn.close();
    }
}
